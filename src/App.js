
import './App.css';
import Header from './component/Header';
import Footer from './component/Footer';
import Home from './component/Home';
import Navbar from './component/Navbar';
import { BrowserRouter, Route } from 'react-router-dom';
import About from './component/About';
import Contact from './component/Contact';

const contactMock = [
  {
    nom: "Mauny",
    prenom: "Matthieu",
    mail: "matthieu@larus.fr"
  },
  {
    nom: "Hardy",
    prenom: "Charlie",
    mail: "Charlie.hardy@gmail.com"
  },
  {
    nom: "Couette",
    prenom: "Beatrice",
    mail: "beatrice.couette@msn.com"
  }
  ,
  {
    nom: "McCool",
    prenom: "Michelle",
    mail: "Michelle.McCool@wwe.com"
  }
];

function App() {
  return (
    <div className="App">
      <Header></Header>
      <BrowserRouter>
        <Navbar></Navbar>
        <Route exact path="/" component={Home}></Route>
        <Route path="/about" component={About}></Route>
        <Route path="/contact" render={(() => <Contact contacts={contactMock}/>)}></Route>
      </BrowserRouter>
      <Footer></Footer>
    </div>
  );
}

export default App;
