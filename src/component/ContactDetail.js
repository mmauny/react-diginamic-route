import { withRouter } from "react-router";

 const findContact = (contacts, nom) => contacts.find(contact => contact.nom === nom)


function ContactDetail({match, contacts}) {
    const contactSelected = findContact(contacts, match.params.nom);
    return (<div>
        <h2>Detail</h2> 
        <ul>
           <li> {contactSelected.nom} </li>
            <li>{contactSelected.prenom}</li>
            <li>{contactSelected.mail}</li>
        </ul>
        </div> )  
}

export default withRouter(ContactDetail);