import { NavLink, Route } from "react-router-dom"
import ContactDetail from "./ContactDetail";

const Contact = ({contacts}) => {
    return <div>
        <ul>
            {contacts.map(contact => <li><NavLink to={`/contact/${contact.nom}`}>{contact.nom}</NavLink></li>)}
        </ul>
        <Route path="/contact/:nom" render={() => <ContactDetail contacts={contacts}></ContactDetail>}></Route>
    </div>
}

export default Contact;

